-- Your SQL goes here

ALTER TABLE todos RENAME TO todos_old;

CREATE TABLE todos (
	id INTEGER PRIMARY KEY NOT NULL,
        item TEXT NOT NULL,
        completed BOOLEAN NOT NULL DEFAULT 0,
	date_created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	date_completed DATETIME
);

INSERT INTO todos SELECT id, item, completed, current_timestamp, NULL FROM todos_old;
DROP TABLE todos_old;
