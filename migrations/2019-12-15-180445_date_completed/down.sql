-- This file should undo anything in `up.sql`
CREATE TEMPORARY TABLE todos_backup(id INTEGER, item TEXT, completed BOOLEAN);
INSERT INTO todos_backup SELECT id, item, completed FROM todos;
DROP TABLE todos;

CREATE TABLE todos (
	id INTEGER PRIMARY KEY NOT NULL,
        item TEXT NOT NULL,
        completed BOOLEAN NOT NULL DEFAULT 0
);

INSERT INTO todos SELECT id, item, completed FROM todos_backup;
DROP TABLE todos_backup;
