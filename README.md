# Very Basic TODO App

This is a simple application for me to learn some [Vue.js](https://vuejs.org/), [Rust](https://www.rust-lang.org/), [Diesel](https://diesel.rs/), [Rocket](https://rocket.rs/) and how they all link together.

The app itself is kind of useless and I wouldn't really use it myself.

These instructions are also kind of pointless, but in case anyone else ever reads this, it might be important to have some vague idea on how to get it up and running.

Some things to note are:
* Uses `Rust` nightly to compile because `Rocket` requires that.
* Uses the `Diesel` CLI program thing to create the database.
* (M)any of the errors do not bubble up to the actual web display, you'll have to check `STDOUT` and/or `STDERR` of the application. I simply haven't done any error checking anywhere.

## Building
1. Clone the repo.
1. Install `rust` if you haven't already. Follow [these instrucations](https://www.rust-lang.org/tools/install)
1. Install the `rust` nightly toolchain for this directory at least. Follow [these instructions](https://rocket.rs/v0.4/guide/getting-started/)
1. Install the `diesel` CLI thingy with at least the sqlite3 backend. Use the command `cargo install diesel_cli`. Read [this page](https://diesel.rs/guides/getting-started/) for some further details.
1. Run `diesel migration run`.
1. Run `cargo run`.
1. Visit the website presented in the terminal. Probably [localhost:8080](localhost:8080)

Please feel free to flick me a message if anything is wrong or just want to ask a question :).
