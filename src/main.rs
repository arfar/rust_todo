#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;
extern crate chrono;
extern crate dotenv;

use chrono::prelude::*;
use diesel::prelude::*;
use rocket::response::Redirect;
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;

mod db;
use db::schema::todos;

#[derive(Serialize, Deserialize, Queryable)]
pub struct Todo {
    pub id: i32,
    pub item: String,
    pub completed: bool,
    pub date_created: NaiveDateTime,
    pub date_completed: Option<NaiveDateTime>,
}

#[get("/all")]
fn get_all_todos() -> Json<Vec<Todo>> {
    use db::schema::todos::dsl::*;
    let db_conn = db::connection::establish_connection();
    Json(todos.load::<Todo>(&db_conn).expect("Error loading posts"))
}

#[get("/<id>")]
fn get_todo(id: i32) -> Json<Todo> {
    use db::schema::todos::dsl::*;
    let db_conn = db::connection::establish_connection();
    Json(todos.find(id).first::<Todo>(&db_conn).unwrap())
}

#[derive(Deserialize, Insertable, Debug)]
#[table_name = "todos"]
pub struct NewTodo<'a> {
    pub item: &'a str,
    pub date_created: Option<NaiveDateTime>,
}

#[post("/", format = "application/json", data = "<new_todo>")]
fn create_todo(new_todo: Json<NewTodo>) -> Json<usize> {
    use db::schema::todos::dsl::*;
    let db_conn = db::connection::establish_connection();
    let mut value = new_todo.into_inner();
    value.date_created = match value.date_created {
        None => Some(Utc::now().naive_utc()),
        Some(d) => Some(d),
    };
    Json(
        diesel::insert_into(todos)
            .values(&value)
            .execute(&db_conn)
            .unwrap(),
    )
}

#[derive(Deserialize, AsChangeset, Debug)]
#[table_name = "todos"]
pub struct UpdateTodo {
    pub item: Option<String>,
    pub completed: Option<bool>,
    pub date_completed: Option<NaiveDateTime>,
}

#[put("/<todo_id>", format = "application/json", data = "<update_todo>")]
fn update_todo(todo_id: i32, update_todo: Json<UpdateTodo>) -> Json<usize> {
    use db::schema::todos::dsl::*;
    let db_conn = db::connection::establish_connection();
    let target = todos.filter(id.eq(todo_id));
    let mut value = update_todo.into_inner();
    if value.completed.is_some() && value.completed.unwrap() && value.date_completed.is_none() {
        value.date_completed = Some(Utc::now().naive_utc());
    }
    Json(
        diesel::update(target)
            .set(&value)
            .execute(&db_conn)
            .unwrap(),
    )
}

#[delete("/<todo_id>")]
fn delete_todo(todo_id: i32) -> Json<usize> {
    use db::schema::todos::dsl::*;
    let db_conn = db::connection::establish_connection();
    Json(
        diesel::delete(todos.filter(id.eq(todo_id)))
            .execute(&db_conn)
            .unwrap(),
    )
}

#[get("/")]
fn index() -> Redirect {
    Redirect::to("/index.html")
}

fn main() {
    rocket::ignite()
        .mount(
            "/api",
            routes![
                get_all_todos,
                get_todo,
                create_todo,
                delete_todo,
                update_todo
            ],
        )
        .mount("/", routes![index])
        .mount("/", StaticFiles::from("html"))
        .launch();
}
