table! {
    todos (id) {
        id -> Integer,
        item -> Text,
        completed -> Bool,
        date_created -> Timestamp,
        date_completed -> Nullable<Timestamp>,
    }
}
